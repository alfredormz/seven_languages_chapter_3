SimpleXMLBuilder := Object clone

SimpleXMLBuilder forward := method(
  tag := call message name
  writeln("<", tag, ">")
  call message arguments foreach(arg,
    content := self doMessage(arg)
    if(content type == "Sequence", writeln(content))
  )
  writeln("</", tag, ">")
)

SimpleXMLBuilder div(
  p(
    "Some Text"
  ),
  div(
    "asdasdasdasd"
  ),
  ul(
    li("Peras"),
    li("Manzanas"),
    li("Otras cosaS")
  )
)
