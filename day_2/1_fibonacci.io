fib := method(n,
  if(n > 1, fib(n-1) + fib(n-2), n)
)

iFib := method(n,
  i := 1
  j := 0
  t := n
  for(k, 0, n-1,
    t := i + j
    i = j
    j = t
  )
  t
)
