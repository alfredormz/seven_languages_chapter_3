doFile("./5_two_dimensional_list.io")
doFile("./6_transpose.io")

m := TwoDimensionalList clone

m dim(2,3)
m set(0, 0, "Dog")
m set(1, 2, "Cat")

m println
m get(1, 2) println

n := TwoDimensionalList dim(2, 3)
n set(0, 0, 23) get(0, 0) println

t := m transpose

(t get(2, 1) == m get(1, 2)) println
