TwoDimensionalList := List clone

TwoDimensionalList dim := method(x, y,

  self setSize(x)

  for(i, 0, x - 1,
    self atPut(i, list setSize(y))
  )

  self
)

TwoDimensionalList set := method(x, y, value,
  self at(x) atPut(y, value)
  self
)

TwoDimensionalList get := method(x, y,
  self at(x) at(y)
)
