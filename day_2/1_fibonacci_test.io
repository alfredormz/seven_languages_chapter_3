doFile("./1_fibonacci.io")

showFibonacciResultsFor := method(n, fn, title,
  writeln(title) 
  writeln

  i := 0
  for(i, 0, n,
    r := call sender perform(fn, i)
    writeln("fib(", i, ") = ", r)
  )

  writeln
)

showFibonacciResultsFor(12, "fib", "Recursive")
showFibonacciResultsFor(12, "iFib", "Iterative")


