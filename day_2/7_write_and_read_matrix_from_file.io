List saveToFile := method(filePath,
  File with(filePath) open write(self serialized) close 
)

List loadFromFile := method(filePath,
  self := doString(
    File with(filePath) open readLines join
  )
)

