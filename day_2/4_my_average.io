List my_average := method(

  try (
    result := self sum / self size
  ) catch (
    writeln("Some elements in the list are not numbers")
  ) pass

  result
)

list(1, 2, 3, 4, 5) my_average println
list(1, 2, 3, 4, "dog", 5) my_average println
