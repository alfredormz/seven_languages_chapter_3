doFile("./5_two_dimensional_list.io")

TwoDimensionalList transpose := method(
  x := self size
  y := self at(0) size

  t := TwoDimensionalList dim(y, x)

  for(i, 0, x - 1,
    for(j, 0, y - 1,
      t set(j, i, self get(i, j))
    )
  )

  t
)

