Number originalDiv := Number getSlot("/")

Number / := method(n,
  if(n==0, 0, self originalDiv(n))
)

(2 / 3) println
(2 / 0) println
