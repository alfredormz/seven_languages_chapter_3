unless := method(
  (call sender doMessage(call message argAt(0))) ifFalse(
  call sender doMessage(call message argAt(1))) ifTrue(
  call sender doMessage(call message argAt(2)))
)

unless(1 == 2, write("One is not two\n"), write("One is two\n"))

unless_alt := method(
  if (call sender doMessage(call message argAt(0))) then (
    call sender doMessage(call message argAt(2))
  ) else (
    call sender doMessage(call message argAt(1))
  )
)

unless_alt(1 == 2, "One is not two" print, "One is two" print)
