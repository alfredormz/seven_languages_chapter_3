doFile("./7_write_and_read_matrix_from_file.io")

t := list(
  list(1,2),
  list(3,4)
)

t saveToFile("7_matrix.data")
r := list loadFromFile("7_matrix.data")

r println

r foreach(row,
  row foreach(col,
    write(col, " ")
  )
  writeln
)
